# Microservices Cloud PoC

PoC project which aims to integrate multiple Microservices implemented using Spring Boot 2 using Service Discovery (Eureka), exposing a common API through a Gateway Service (Zuul).
It also aims to integrate [Spring Boot Admin](https://github.com/codecentric/spring-boot-admin) and [Spring Boot Dashboard](https://github.com/ordina-jworks/microservices-dashboard-server/tree/master/samples) based on [Spring Tips: Bootiful Dashboards](https://www.youtube.com/watch?v=u1QnlAbCFys&t=3s).

## Development Note

Spring Boot Dashboard is not functional yet.


## Build

All project can be built by using command:

    $ ./mvnw package

## Run

After built, all Services may be started in Docker containers by using:

    $ docker-compose up --force-recreate --remove-orphans --build


By now all Services have ports mapped from Docker container to host machine. The idea is that the only entry point whole infrastructure should be through Gateway Service.
This is why this is the only Service with a fixed mapped port, `8080`, while other Service are mapped to random host ports.

## Scaling

One of the goals of this PoC is to experiment how easy it is to scale a particular Service by creating replicas of this Service, and also how the infrastructure (service discovery, load balancing) deals with that.

Example, when requesting path `/api/agenda/reservations/9`, the request first pass through the Gateway Service, which forwards to Agenda Service. Agenda Services loads a record of Reservation, and for each Participant, requests CRM Service to retrieve information about the Person. Finally resulting in:

    $ curl -i http://localhost:8080/api/agenda/reservations/9
    HTTP/1.1 200 
    Date: Sun, 25 Mar 2018 16:38:44 GMT
    Content-Type: application/json;charset=UTF-8
    Transfer-Encoding: chunked
    
    {
      "id": 9,
      "startAt": "2018-01-12T02:00:00Z",
      "endAt": "2018-01-14T05:30:00Z",
      "participants": [
        {
          "id": 1,
          "name": "John Lennon",
          "birthday": "1940-10-09"
        },
        {
          "id": 3,
          "name": "Ringo Starr",
          "birthday": "1940-07-07"
        },
        {
          "id": 2,
          "name": "Paul McCartney",
          "birthday": "1942-06-18"
        },
        {
          "id": 4,
          "name": "George Harrison",
          "birthday": "1943-02-25"
        }
      ]
    }

This flow may be seen in docker compose logs (`docker-compose logs -f`):

    agenda-service_1     | 2018-03-25 16:38:44.722 DEBUG 1 --- [nio-8080-exec-8] b.f.m.agenda.api.ReservationsController  : loadOne(id: 9)
    crm-service_1        | 2018-03-25 16:38:44.781 DEBUG 1 --- [nio-8080-exec-2] b.f.m.crm.api.PersonsController          : loadById(id: 1)
    crm-service_1        | 2018-03-25 16:38:44.794 DEBUG 1 --- [nio-8080-exec-7] b.f.m.crm.api.PersonsController          : loadById(id: 3)
    crm-service_1        | 2018-03-25 16:38:44.809 DEBUG 1 --- [io-8080-exec-13] b.f.m.crm.api.PersonsController          : loadById(id: 2)
    crm-service_1        | 2018-03-25 16:38:44.817 DEBUG 1 --- [io-8080-exec-11] b.f.m.crm.api.PersonsController          : loadById(id: 4)

Now, if we scale CRM Service to 4 instances (`docker-compose scale crm-service=4`) and perform the same request again, it may be noticed the requests from Agenda Service to CRM Services are automatically handled by different instances of CRM Service:

    agenda-service_1     | 2018-03-25 16:44:16.037 DEBUG 1 --- [nio-8080-exec-2] b.f.m.agenda.api.ReservationsController  : loadOne(id: 9)
    crm-service_4        | 2018-03-25 16:44:16.290 DEBUG 1 --- [nio-8080-exec-5] b.f.m.crm.api.PersonsController          : loadById(id: 1)
    crm-service_1        | 2018-03-25 16:44:16.358 DEBUG 1 --- [io-8080-exec-13] b.f.m.crm.api.PersonsController          : loadById(id: 3)
    crm-service_3        | 2018-03-25 16:44:16.565 DEBUG 1 --- [io-8080-exec-10] b.f.m.crm.api.PersonsController          : loadById(id: 2)
    crm-service_2        | 2018-03-25 16:44:16.885 DEBUG 1 --- [nio-8080-exec-6] b.f.m.crm.api.PersonsController          : loadById(id: 4) 

## Discovery Service (Eureka) UI

Discovery Service, implemented using Eureka, provides a UI that can be accessed through http://localhost:8761/.
