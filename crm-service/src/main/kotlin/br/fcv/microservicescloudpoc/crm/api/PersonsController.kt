package br.fcv.microservicescloudpoc.crm.api

import br.fcv.microservicescloudpoc.crm.model.Person
import com.google.common.collect.ImmutableList
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant
import java.time.LocalDate
import java.time.Month

/**
 * @author fabio.veronez@gmail.com
 */
@RestController
@RequestMapping("/persons")
class PersonsController {

    @GetMapping
    fun loadAll(): List<Person> {

        logger.debug("loadAll()")
        return people
    }

    @GetMapping("/{id}")
    fun loadById(@PathVariable id: Long): ResponseEntity<Person> {
        logger.debug("loadById(id: {})", id)
        return people.find { id == it.id }
                ?.let { ResponseEntity.ok(it) }
                ?: ResponseEntity.notFound().build()
    }

    companion object {
        val people = ImmutableList.of(
                Person(1L, "John Lennon", LocalDate.of(1940, Month.OCTOBER, 9)),
                Person(2L, "Paul McCartney", LocalDate.of(1942, Month.JUNE, 18)),
                Person(3L, "Ringo Starr", LocalDate.of(1940, Month.JULY, 7)),
                Person(4L, "George Harrison", LocalDate.of(1943, Month.FEBRUARY, 25))
        )!!

        val logger = LoggerFactory.getLogger(PersonsController::class.java)
    }

}

