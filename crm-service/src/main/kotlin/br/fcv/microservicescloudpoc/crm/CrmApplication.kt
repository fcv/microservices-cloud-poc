package br.fcv.microservicescloudpoc.crm

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

/**
 * @author fabio.veronez@gmail.com
 */
@EnableDiscoveryClient
@SpringBootApplication
class CrmApplication

fun main(args: Array<String>) {
    runApplication<CrmApplication>(*args)
}
