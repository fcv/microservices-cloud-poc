package br.fcv.microservicescloudpoc.agenda.api

import br.fcv.microservicescloudpoc.agenda.PersonsClient
import br.fcv.microservicescloudpoc.agenda.model.Person
import br.fcv.microservicescloudpoc.agenda.model.Reservation
import com.google.common.collect.ImmutableList
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

/**
 * @author fabio.veronez@gmail.com
 */
@RestController
@RequestMapping("/reservations")
class ReservationsController(val personClient: PersonsClient) {

    @GetMapping
    fun loadAll(): List<Reservation> {

        logger.debug("loadAll")
        return reservations
    }

    @GetMapping("/{id}")
    fun loadOne(@PathVariable id: Long): ResponseEntity<ReservationWithParticipants> {

        logger.debug("loadOne(id: {})", id)
        return reservations
                .find { it.id == id }
                ?.let {
                    ReservationWithParticipants(it.id,
                            it.startAt,
                            it.endAt,
                            it.participantIds.map(personClient::loadPerson))
                }
                ?.let { ResponseEntity.ok(it) }
                ?: ResponseEntity.notFound().build()
    }

    companion object {

        val reservations: List<Reservation> = ImmutableList.of(
                Reservation(5L, Instant.parse("2018-03-18T20:00:00Z"), Instant.parse("2018-03-18T21:00:00Z"), listOf(1L, 2L)),
                Reservation(7L, Instant.parse("2018-01-10T02:00:00Z"), Instant.parse("2018-01-10T05:30:00Z"), listOf(1L, 4L)),
                Reservation(9L, Instant.parse("2018-01-12T02:00:00Z"), Instant.parse("2018-01-14T05:30:00Z"), listOf(1L, 3L, 2L, 4L))
        )

        val logger = LoggerFactory.getLogger(ReservationsController::class.java)
    }

    data class ReservationWithParticipants(val id: Long?, val startAt: Instant, val endAt: Instant, val participants: List<Person>)
}