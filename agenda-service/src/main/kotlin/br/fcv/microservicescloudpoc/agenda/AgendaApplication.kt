package br.fcv.microservicescloudpoc.agenda

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.openfeign.EnableFeignClients

/**
 * @author fabio.veronez@gmail.com
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
class AgendaApplication

fun main(args: Array<String>) {
    runApplication<AgendaApplication>(*args)
}
