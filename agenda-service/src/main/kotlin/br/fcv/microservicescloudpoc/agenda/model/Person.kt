package br.fcv.microservicescloudpoc.agenda.model

import java.time.LocalDate

/**
 * @author fabio.veronez@gmail.com
 */
data class Person(val id: Long?, val name: String, val birthday: LocalDate)