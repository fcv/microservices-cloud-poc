package br.fcv.microservicescloudpoc.agenda.model

import java.time.Instant

/**
 * @author fabio.veronez@gmail.com
 */
data class Reservation(val id: Long?, val startAt: Instant, val endAt: Instant, val participantIds: List<Long>)