package br.fcv.microservicescloudpoc.agenda

import br.fcv.microservicescloudpoc.agenda.model.Person
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

/**
 * @author fabio.veronez@gmail.com
 */
@FeignClient("\${microservices-cloud-poc.services.crm-service.application-name}")
interface PersonsClient {

    @GetMapping("/persons/{id}")
    fun loadPerson(@PathVariable id: Long): Person

}